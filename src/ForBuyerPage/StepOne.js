import React from "react";
import styled from "styled-components";
import StyledButton from "../assets/StyledButton";
import Input from '@material-ui/core/Input';
import { ReactComponent as ScanQrIcon } from '../assets/Icons/ic_scan_qrcode.svg';

const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const BlueText = styled.div`
    font-family: 'SukhumvitSet-Semibold';
    color: #007AFF;
    font-size: 1.25rem;
    letter-spacing: 0.02rem;
    line-height: 1.5rem;
    margin: 2rem auto;
`;
const AddrDiv = styled.div`
  display: flex;
  margin: 3rem auto 5rem;
`;
const AddressInput = styled(Input)`
  && {
    width: 16.69rem;
    height: 2.75rem;
    cursor: pointer;
    @media (max-width: 374px) {
      width: 15rem;
    }
  }
  &.MuiInput-underline:before {
    border-bottom: 0.7px solid rgba(0, 0, 0, 0.3);
  }
  &.MuiInput-underline:after {
    border-bottom: none;
  }
`;
const ScanQrIc = styled(ScanQrIcon)`
  width: 2.63rem;
  height: 2.63rem;
  margin-left: 0.16rem;
  @media (max-width: 374px) {
    width: 2rem;
    height: 2rem;
  }
`;

export default function StepOne(){
    return(
        <Root>
            <BlueText>PAY to</BlueText>
            <AddrDiv>
                <AddressInput
                    placeholder="Seller Address"
                    inputProps={{ "aria-label": "description" }}
                />
                <ScanQrIc/>
            </AddrDiv>
            <StyledButton blue="true">Next</StyledButton>
        </Root>
    )
}

