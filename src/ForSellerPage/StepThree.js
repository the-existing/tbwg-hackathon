import React from "react";
import styled from "styled-components";
import { ReactComponent as SuccessIcon } from '../assets/Icons/ic_success.svg';
import StyledButton from "../assets/StyledButton";

const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: calc(100vh - 10rem);
`;
const SuccessIc = styled(SuccessIcon)`
    width: 6.25rem;
    height: 7.38rem;
`
const GrayText = styled.div`
    font-family: 'SukhumvitSet-Text';
    color: #3C3C43;
    opacity: 0.6;
    font-size: 1.25rem;
    letter-spacing: 0.02rem;
    line-height: 1.5rem;
    margin: 1rem auto;
`
const GradientText = styled.h1`
    font-size: 2.75rem;
    font-family: 'SukhumvitSet-Text';
    background: -webkit-linear-gradient(#FF8BA2, #C263BF );
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    margin: 0 auto 2.63rem;
    line-height: 2.75rem;
    letter-spacing: 0.05rem;
`

export default function StepThree(){
    return(
        <Root>
            <SuccessIc/>
            <GrayText>Payment</GrayText>
            <GradientText>​Success</GradientText>
            <StyledButton>Finish</StyledButton>
        </Root>
    )
}
