import React from "react";
import styled from "styled-components";
import { ReactComponent as DownloadIcon } from '../assets/Icons/ic_download.svg';
import { ReactComponent as ShareIcon } from '../assets/Icons/ic_share.svg';
import { ReactComponent as CopyIcon } from '../assets/Icons/ic_copy.svg';
import colors from '../assets/Color';
import StyledButton from "../assets/StyledButton";
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment';
import IconAdorn from '../assets/IconAdornment';

const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const DownloadIc = styled(DownloadIcon)`
    font-size: 2.63rem;
    cursor: pointer;
`
const ShareIc = styled(ShareIcon)`
    font-size: 2.63rem;
    cursor: pointer;
`
const CopyIc = styled(CopyIcon)`
    font-size: 2.63rem;
    cursor: pointer;
`
const GrayText = styled.div`
    font-family: 'SukhumvitSet-Text';
    color: #3C3C43;
    opacity: 0.6;
    font-size: 1.25rem;
    letter-spacing: 0.02rem;
    line-height: 1.5rem;
`
const PriceBox = styled.div`
    width: 19.5rem;
    height: 4.38rem;
    border-radius: 0.5rem;
    background: #F5F5F5;
    font-family: 'SukhumvitSet-Text';
    font-size: 1.75rem;
    letter-spacing: 0.02rem;
    line-height: 2.13rem;
    color: ${colors.black};
    display: flex;
    justify-content:center;
    align-items: center;
    @media (max-width: 374px) {
      width: 17.2rem;
    }
`
const SellerTab = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 19.5rem;
    margin: 2rem auto 1rem;
    @media (max-width: 374px) {
      width: 17.2rem;
    }
`
const QrDiv = styled.div`
    width: 18.94rem;
    height: 18.94rem;
`
const StyledTextField = styled(TextField)`
    && {
        width: 19.5rem;
        margin-bottom: 1.31rem;
    }
    & .MuiInputBase-input {
        height: 2.75rem;
    }
    & .MuiInput-underline:before {
        border-bottom: 0.7px solid rgba(0, 0, 0, 0.3);
    }
    & .MuiInput-underline:after {
        border-bottom: none;
    }
`

export default function StepTwo(){
    return(
        <Root>
            <SellerTab>
                <GrayText>For Seller</GrayText>
                <div>
                    <DownloadIc/>
                    <ShareIc/>
                </div>
            </SellerTab>
            <PriceBox>2.23 ETH</PriceBox>
            <QrDiv>ตรงนี้จะเป็น QR Code</QrDiv>
            <StyledTextField
                id="input-with-icon-textfield"
                InputProps={{
                startAdornment: (
                    <InputAdornment position="start"><IconAdorn disabled/></InputAdornment>
                ),
                endAdornment: (
                    <InputAdornment position="end">
                        <CopyIc/>
                    </InputAdornment>
                ),
                }}
            />
            <StyledButton blue="true">Confirm Payment</StyledButton>
        </Root>
    )
}