import React from "react";
import styled from "styled-components";
import { ReactComponent as SellerIcon } from "../assets/Icons/ic_seller.svg";
import { ReactComponent as ScanQrIcon } from "../assets/Icons/ic_scan_qrcode.svg";
import Input from "@material-ui/core/Input";
import TextField from "@material-ui/core/TextField";
import StyledButton from "../assets/StyledButton";

const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const SellerIc = styled(SellerIcon)`
  font-size: 4.09rem;
  margin: 3rem auto;
`;
const SellerAddrDiv = styled.div`
  display: flex;
`;
const AddressInput = styled(Input)`
  && {
    width: 16.69rem;
    height: 2.75rem;
    @media (max-width: 374px) {
      width: 15rem;
    }
  }
  &.MuiInput-underline:before {
    border-bottom: 0.7px solid rgba(0, 0, 0, 0.3);
  }
  &.MuiInput-underline:after {
    border-bottom: none;
  }
`;
const ScanQrIc = styled(ScanQrIcon)`
  width: 2.63rem;
  height: 2.63rem;
  margin-left: 0.16rem;
  @media (max-width: 374px) {
    width: 2rem;
    height: 2rem;
  }
`;
const PriceField = styled(TextField)`
  && {
    margin: 3rem auto 3.5rem auto;
    width: 19.5rem;
    height: 2.75rem;
    @media (max-width: 374px) {
      width: 17.2rem;
    }
  }
  & .MuiFilledInput-root {
    background-color: #f2f2f7;
  }
  & .MuiFilledInput-root:hover {
    background-color: #f2f2f7;
  }
  & .MuiFilledInput-input {
    padding: 16px 12px 10px;
  }
  & .MuiFilledInput-underline:before {
    border-bottom: 0.7px solid rgba(0, 0, 0, 0.3);
  }
  & .MuiFilledInput-underline:after {
    border-bottom: none;
  }
  & .MuiInputBase-input {
    text-align: right;
  }
`;

export default function StepOne() {
  return (
    <Root>
      <SellerIc />
      <SellerAddrDiv>
        <AddressInput
          placeholder="Your wallet address"
          inputProps={{ "aria-label": "description" }}
        />
        <ScanQrIc />
      </SellerAddrDiv>
      <PriceField
        id="price"
        defaultValue="0.00"
        variant="filled"
        type="number"
      />
      <StyledButton blue="true">Generate Quick PAY</StyledButton>

      <StyledButton style={{ bottom: 10, position: "fixed" }}>
        Install VerME on this device
      </StyledButton>
    </Root>
  );
}
