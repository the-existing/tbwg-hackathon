import React from "react";
import styled from "styled-components";
import Header from "../Header";
import StepOne from "./StepOne";
import StepTwo from './StepTwo';
import StepThree from './StepThree';

const Container = styled.div`
  /* height: 100vh; */
`;

export default function ForSeller() {
  return (
    <div>
      <Header />
      <Container>
        {/* <StepOne/> */}
        {/* <StepTwo/> */}
        <StepThree/>
      </Container>
    </div>
  );
}
