import Button from '@material-ui/core/Button'
import styled from 'styled-components'
// import colors from '../assets/Color'

const StyledButton = styled(Button)`
    && {
        width: 19.5rem;
        height: 2.75rem;
        background: ${props => props.blue ? "#007AFF " : "transparent"};
        font-family: 'SukhumvitSet-SemiBold';
        font-size: 1.25rem;
        color: ${props => props.blue ? "white" : "#007AFF" };
        letter-spacing: 0.08rem;
        line-height: 1rem;
        border-radius: 0.25rem;
        border: none;
        box-shadow: ${props => props.blue ? "0 0.06rem 0.06rem 0 rgba(0,0,0,0.50)" : ""};
        &:hover {
            background: ${props => props.blue ? "#026bde" : "transparent"};
        }
        /* margin-right: ${props => props.blue ? "0" : "1rem"}; */
        @media (max-width: 374px){
            width: 17.2rem;
        }
    }
    &.MuiButton-root{
        text-transform: none;
    }
`

export default StyledButton;