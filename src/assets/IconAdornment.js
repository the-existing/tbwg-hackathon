import IconButton from '@material-ui/core/IconButton'
import styled from 'styled-components'

const IconAdorn = styled(IconButton)`
    && {
        opacity: 0;
        width: 1rem;
    }
`
export default IconAdorn;