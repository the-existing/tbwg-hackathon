import React from 'react';
import styled from 'styled-components';
import { ReactComponent as Logo } from './assets/Logo/logo_verme_dark.svg';
import BackButton from './BackButton';

const HeaderDiv = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 4rem;
    width: 100%;
`
const GrayText = styled.div`
    font-family: 'SukhumvitSet-Text';
    color: #3C3C43;
    opacity: 0.6;
    font-size: 1.25rem;
    letter-spacing: 0.02rem;
    line-height: 1.5rem;
    margin-left: 8%;
`
const StyledLogo = styled(Logo)`
    width: 6.63rem;
    height: 1.63rem;
    margin-right: 8%;
`

export default function Header() {
    return (
            <HeaderDiv>
                {/* <GrayText>For Seller</GrayText> */}
                <BackButton/>
                {/* <GrayText>For Buyer</GrayText> */}
                {/* <div style={{width: '1rem',opacity: 0}}>verme</div> */}
                <StyledLogo/>
            </HeaderDiv>
    )
}