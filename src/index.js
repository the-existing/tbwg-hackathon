import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import ForSeller from './ForSellerPage/ForSeller';
import ForBuyer from './ForBuyerPage/ForBuyer'
// import ErrorNotFound from './404notfound'

ReactDOM.hydrate(<BrowserRouter>
    <div>
      <Switch>
        <Route path='/for-seller' component={ForSeller}/>
        <Route path='/for-buyer' component={ForBuyer}/>
        {/* <Route component={ErrorNotFound} /> */}
      </Switch>
    </div>
</BrowserRouter>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
